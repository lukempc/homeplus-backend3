package com.homeplus.controllers;

import com.homeplus.dtos.follow.FollowGetDto;
import com.homeplus.dtos.follow.FollowPostDto;
import com.homeplus.services.TaskFollowService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@Validated
public class TaskFollowController {

    private final TaskFollowService taskFollowService;

    @GetMapping("user-follows")
    public ResponseEntity<List<FollowGetDto>> findUserFollows(@RequestParam Long id) {
        return ResponseEntity.ok(taskFollowService.getFollowsByUserId(id));
    }

    @PostMapping("/follow")
    public ResponseEntity createFollow(@Valid @RequestBody FollowPostDto followPostDto) {
        taskFollowService.createFollow(followPostDto);
        return ResponseEntity.ok("success");
    }
}
