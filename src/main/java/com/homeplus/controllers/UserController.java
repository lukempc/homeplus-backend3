package com.homeplus.controllers;

import com.homeplus.dtos.*;
import com.homeplus.dtos.user.UserGetDto;
import com.homeplus.dtos.user.UserPutDto;
import com.homeplus.services.UserService;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@Validated
@EnableAsync
public class UserController {

    private final UserService userService;

    @Value("${jwt.secretKey}")
    private String secretKey;

    @GetMapping("/signup")
    public ResponseEntity<String> verifyEmailExists(@RequestParam(value = "email") String email) {
        if (userService.ifEmailExists(email)) {
            return new ResponseEntity<>("Email has taken", HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>("Email does not exist", HttpStatus.OK);
    }

    @PostMapping("/signup")
    public ResponseEntity createUser(@Valid @RequestBody AccountDto accountDto) {
        log.info("email: {}, name: {}", accountDto.getEmail(), accountDto.getName());
        userService.register(accountDto);
        return ResponseEntity.ok("success");
    }

    @GetMapping("/isVerified")
    public ResponseEntity verifyStatus(@RequestParam(value = "email") String email) {
        log.info("email: {}", email);
        if (userService.findVerifyStatus(email).equals("UNVERIFIED")) {
            return new ResponseEntity<>("Unverified user", HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        }
        return ResponseEntity.ok("success");
    }

    @PutMapping(path = "/confirm")
    public String confirm(@RequestParam("token") String token) {
        log.info("Token in controller is: " + token);
        return userService.confirmEmailToken(token);
    }

    @GetMapping("/login")
    public ResponseEntity loginUserThenReturnJwtToken(@RequestParam("email") String email, @RequestParam("password") String password) {
        log.info("email: {}", email);
        if (!userService.isUserExists(email)) {
            return new ResponseEntity<>("Email not exists", HttpStatus.NOT_FOUND);
        }
        if (userService.findVerifyStatus(email).equals("UNVERIFIED")) {
            return new ResponseEntity<>("Unverified user", HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        }
        if(!userService.isPasswordCorrect(email, password)){
            return new ResponseEntity<>("Login failed, please check your email or password.", HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        };
        String jwtToken = userService.generateJws(email, this.secretKey);
        return ResponseEntity.ok(jwtToken);
    }

    @GetMapping("/auth")
    public ResponseEntity JwtAuthorizationCheck(@RequestParam("jwt") String jwt) {
        Boolean isValid = userService.isJwtValid(jwt, secretKey);
        return ResponseEntity.ok(isValid);
    }

    @GetMapping("/getUser")
    public ResponseEntity getUserGetDtoByEmail(@RequestParam(value = "jwt") String jwt) {
        if(!userService.validateJwt(jwt, this.secretKey)){
            return new ResponseEntity<>("Not proper jwt token", HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        }
        String email = userService.getEmailFromJwtToken(jwt, this.secretKey);

        UserGetDto userGetDto = userService.findUserGetDtoByEmail(email);
        return ResponseEntity.ok(userGetDto);
    }

    //====================================
    @GetMapping(path = "/testJwt")
    public String testJwtToken(@RequestParam(value = "email") String email) {

        String jwt = userService.generateJws(email, this.secretKey);
        return jwt;
    }

    @GetMapping(path = "/parseJwt")
    public ResponseEntity parseJwt(@RequestParam(value = "jwt") String jwt) {

        Claims body = userService.parseJwt(jwt, this.secretKey);
        return ResponseEntity.ok(body);
    }

// TODO: EMAIL VERIFY LINK NEED TO HAVE AN EXPIRE TIME.

    @GetMapping(path = "/user")
    public ResponseEntity<UserGetDto> getUser(@RequestParam(value = "id") Long id) {
        return ResponseEntity.ok(userService.getUserDtoById(id));
    }

    @PutMapping(path = "/on-off-tasker")
    public ResponseEntity<String> turnOnOffTasker(@RequestParam(value = "id") Long id) {
        userService.turnOnOffTasker(id);
        return ResponseEntity.ok("success");
    }

    @PutMapping(path = "/is-tasker-data")
    public ResponseEntity<String> setIsTaskerData(@RequestParam(value = "id") Long id) {
        userService.updateIsTaskerData(id);
        return ResponseEntity.ok("success");
    }

    @PutMapping(path = "/update-user")
    public ResponseEntity<String> turnOnOffTasker(@Valid @RequestBody UserPutDto userPutDto) {
        userService.updateUser(userPutDto);
        return ResponseEntity.ok("success");
    }

//    @PostMapping("/resend")
//    public ResponseEntity resendActivationLink(@Valid @RequestBody UserInfoDto userInfoDto) {
//        userService.generateVerifyLink(userInfoDto.getEmail());
//        return ResponseEntity.ok("success");
//    }

//    @PostMapping("/verify")
//    public ResponseEntity verifyActiveUser(@RequestParam(value = "code") String code) throws URISyntaxException {
//        log.info(code);
//        boolean isVerified = userService.isAccountActivated(code);
//        if (isVerified) {
//            return ResponseEntity.ok("success");
//        }
//        return new ResponseEntity<>("Inactivated", HttpStatus.NON_AUTHORITATIVE_INFORMATION);
//    }
}
