package com.homeplus.repositories;

import com.homeplus.models.TaskEntity;
import com.homeplus.models.TaskFollowEntity;
import com.homeplus.models.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
@EnableJpaRepositories
public interface TaskFollowRepository extends JpaRepository<TaskFollowEntity, Long> {

    @Query("select f from TaskFollowEntity as f where f.id = :id")
    Optional<TaskFollowEntity> findById(@Param("id") Long id);

    @Query("select f from TaskFollowEntity as f where (f.userEntity = :userEntity) and (f.following = true)")
    List<TaskFollowEntity> findByUser(@Param("userEntity") UserEntity userEntity);

    @Query("select f from TaskFollowEntity as f where " +
            "(f.userEntity = :userEntity) and " +
            "(f.taskEntity = :taskEntity)")
    Optional<TaskFollowEntity> findByUserAndTask(@Param("userEntity") UserEntity userEntity, @Param("taskEntity") TaskEntity taskEntity);
}
