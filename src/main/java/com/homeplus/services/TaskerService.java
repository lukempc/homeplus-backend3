package com.homeplus.services;

import com.homeplus.dtos.task.TaskGetDto;
import com.homeplus.dtos.tasker.TaskerGetDto;
import com.homeplus.dtos.tasker.TaskerPostDto;
import com.homeplus.dtos.tasker.TaskerPutDto;
import com.homeplus.models.TaskerEntity;
import com.homeplus.models.UserEntity;
import com.homeplus.repositories.TaskerRepository;
import com.homeplus.repositories.UserRepository;
import com.homeplus.utility.mapper.TaskerMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskerService {

    private final TaskerRepository taskerRepository;
    private final TaskerMapper taskerMapper;
    private final UserService userService;

    public List<TaskerGetDto> getAllTaskers() {
        return taskerRepository.findAll().stream()
                .map(task -> taskerMapper.fromEntity(task))
                .collect(Collectors.toList());
    }

    public TaskerGetDto getTaskerById(Long id) {
        return taskerMapper.fromEntity(getTaskerEntityById(id));
    }

    public TaskerEntity getTaskerEntityById(Long id){
        return taskerRepository.findById(id).get();
    }

    public void createTasker(TaskerPostDto taskerPostDto) {
        UserEntity user = userService.getUserById(taskerPostDto.getUser_id());
        Boolean taskerExist = taskerRepository.findByUser(user).isPresent();
        if(taskerExist) {
            throw new IllegalStateException("This user already has tasker data");
        }

        taskerPostDto.setUserEntity(user);
        TaskerEntity taskerEntity = taskerMapper.postDtoToEntity(taskerPostDto);
        taskerRepository.save(taskerEntity);

    }


    public void updateTasker(TaskerPutDto taskerPutDto) {
        UserEntity user = userService.getUserById(taskerPutDto.getUserEntity().getId());
        Boolean taskerExist = taskerRepository.findByUser(user).isPresent();
        if(taskerExist) {
            taskerRepository.save(taskerMapper.putDtoToEntity(taskerPutDto));
        }
    }

    public TaskerGetDto getTaskerByUserId(Long id) {
        UserEntity user = userService.getUserById(id);
        return taskerMapper.fromEntity(taskerRepository.findByUser(user).get());
    }
}
